/**
* Print command-line parameters.
*/
public class Main {
/**
* Program entry point.
* @param args command-line parameters list
*/

5

public static void main(String[] args) {
        
        for (String s: args) {
            System.out.println(s);
        }
    }
}
